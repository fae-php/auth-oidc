<?php

namespace FAE\auth_oidc;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

use FAE\fae\routes;

global $config;

$authRoutes = new RouteCollection();
$authRoutes->add('auth-oidc-api-keycloak', new Route(
  "/api/{$config->apiVersion}/auth_oidc/keycloak/authorize",
  [
    '_controller'   => '\\FAE\\auth_oidc\\keycloak::apiLogin',
  ],
  [],
  [],
  '',
  [],
  ['GET']
));

routes::addCollection($authRoutes);
