<?php

namespace FAE\auth_oidc;

use FAE\auth\authInterface;
use FAE\auth\authProvider;

class auth extends authProvider
{
  // Provider interface for the OIDC - configurable through $config->oidcProvider
  static $provider;

  /**
   * Return the configured provider interface
   *
   * @var $config->oidcProvider   (string) A class reference to the OIDC provider. Must extend the \League\OAuth2\Client\Provider\AbstractProvider interface
   * @var $config->oidcSettings   (array) An array of settings to be passed to the OIDC provider
   * @return oidcAbstract         Returns an implementation of the oidcAbstract based on configured providers
   */
  public function getProvider()
  {
    global $config;

    if (!self::$provider) {
      $providerClass = property_exists($config, 'oidcProvider') ? $config->oidcProvider : 'oidc';
      try {
        self::$provider = new $providerClass($config->oidcSettings ?: []);
      } catch (\Error $e) {
        var_dump("Could not instantiate OIDC provider '{$providerClass}'");
        throw new \Error($e);
      }
    }

    return self::$provider;
  }

  public function getSession()
  {
    $return = $this->getProvider()->getSession();
    $this->user_id = $this->getProvider()->getUserID();
    $this->perm_group_id = $this->getProvider()->getPermGroup();
    $this->username = $this->getProvider()->getUsername();
    return $return;
  }

  public function redirectLogin()
  {
    return $this->getProvider()->redirectLogin();
  }

  public function loginURL(?string $destination = NULL)
  {
    return $this->getProvider()->loginURL();
  }
}
