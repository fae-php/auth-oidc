<?php

namespace FAE\auth_oidc;

use FAE\auth\authException;
use FAE\user\user;
use Firebase\JWT\{ JWT, ExpiredException, Key };
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;

abstract class oidcAbstract extends GenericProvider
{

  protected $user_id;
  protected $username;
  protected $perm_group_id;
  
  // JWT time validation leeway in seconds
  protected $leeway = 10;

  public function getSession(): bool
  {
    if ($jwt = $this->getBearerToken()) {
      $this->user_id = $this->loginFromJWT($jwt);
    } elseif (isset($_SESSION['user_id'])) {
      $this->user_id = $_SESSION['user_id'];
    }

    if (!$this->user_id) {
      return false;
    }

    $uh = new user();
    $uh->setDirect(true);
    $user = (object) $uh->getById($this->user_id);
    $this->username = $user->username;
    $this->perm_group_id = $user->perm_group_id;

    return true;
  }

  public function getUserID(): ?int
  {
    return $this->user_id;
  }

  public function getUsername(): ?string
  {
    return $this->username;
  }

  public function getPermGroup(): ?int
  {
    return $this->perm_group_id;
  }

  public function createUser(object $token)
  {
    $username   = $token->sub;
    $email      = $token->email;
    $firstname  = $token->given_name;
    $lastname   = $token->family_name;


    if(!$username){
      throw new authException("Authentication token did not include a sub");
    }
    if(!$email){
      throw new authException("Authentication token did not include an email");
    }

    $uh = new user();
    $uh->setDirect(true);
    $user = $uh->insert([
      'username'    => $username,
      'email'       => $email,
      'first_name'   => $firstname,
      'last_name'    => $lastname,
      'perm_group_id' => auth::getDefaultPermGroup(),
    ]);

    return $user['id'];
  }

  public function getBearerToken(): ?string
  {
    $header = $_SERVER['HTTP_AUTHORIZATION'];
    if (!$header) {
      return false;
    }

    return preg_replace("/Bearer\s+/", "", $header);
  }

  public function decodeJWT(string $jwt): object
  {
    list($header, $payload, $signature) = explode(".", $jwt);

    $header = json_decode(JWT::urlsafeB64Decode($header), true);

    if (empty($header)) {
      throw new \RuntimeException('Sorry, token provided is not a valid JWT');
    }

    $algo = ['RS256', $header['alg']];
    
    JWT::$leeway = $this->leeway;

    $decoded = JWT::decode($jwt, new Key( $this->getPubkey(), $header['alg'] ));

    return $decoded;
  }

  /**
   * Find the user id associated with this keycloak token (usually based on email address)
   * @param string $keycloakID
   */
  public function getUserFromOIDCToken(string $jwt): ?int
  {
    try {
      $decoded = $this->decodeJWT($jwt);
    } catch (\RuntimeException $e) {
      throw new authException($e->getMessage(), $e->getCode(), $e);
    }

    $sub = $decoded->sub;

    $uh = new user();
    $uh->setDirect(true);
    // This is the first place that will trigger a loop if setDirect is not taking effect
    $user = $uh->get(['username' => $sub]);
    if ($user->rowCount()) {
      return $user->fetch()['id'];
    }

    return null;
  }

  /**
   * Generate login URL
   *
   * @param string $destination   Destination is not supported for OIDC login
   * @return string               Authentication URL
   */
  public function loginURL(?string $destination = null): string
  {
    $options = [];
    return $this->getAuthorizationUrl($options);
  }

  private function currentPage()
  {
    global $config;
    return $config->root . $config->path . preg_replace('/^' . preg_quote($config->path, '/') . '/', '', $_SERVER['REQUEST_URI']);
  }

  public function redirectLogin()
  {
    // Get the login URL, state is generated at point of link generation so must be done first
    $loginUrl = $this->loginURL();

    // Get the current page and store it as the destination
    $_SESSION['oidc_dest'] = $this->currentPage();

    // Get the state generated for you and store it to the session.
    $_SESSION['oauth2state'] = $this->getState();

    // Redirect the user to the authorization URL.
    header('Location: ' . $loginUrl);
    exit;
  }

  static function apiLogin(): void
  {
    $ah = new auth();
    $provider = $ah->getProvider();

    $code           = $_GET['code'];
    $state          = $_GET['state'];
    //$session_state  = $_GET['session_state'];
    $dest           = $_SESSION['oidc_dest'];

    if (!$code) {
      $ah->redirectLogin();
      return;
    }

    if (empty($state) || !isset($_SESSION['oauth2state']) || $state !== $_SESSION['oauth2state']) {
      if (isset($_SESSION['oauth2state'])) {
        unset($_SESSION['oauth2state']);
      }
      throw new authException('Invalid state');
      return;
    }

    $jwt = $provider->parseCode($code);
    $_SESSION['user_id'] = $provider->loginFromJWT($jwt);

    header("Location: " . $dest);
    exit;
  }

  /**
   * Login user from java web token
   *
   * @param string $jwt
   * @return integer|null  UserID
   */
  public function loginFromJWT(string $jwt): ?int
  {
    try {
      $uid = $this->getUserFromOIDCToken($jwt);
    } catch (\RuntimeException $e) {
      throw new authException($e->getMessage(), $e->getCode(), $e);
    }

    if (empty($uid)) {
      //$this->log->debug("No user found for this token, creating new user");
      $profile = $this->decodeJWT($jwt);
      $uid = $this->createUser($profile);
      //$this->log->debug("Created new user ($uid)");
    } else {
      //$this->log->debug("User for $uid found");
    }

    if (empty($uid)) {
      throw new \Exception("Could not create a new user account");
      return null;
    }

    return $uid;
  }

  /**
   * Verify a JWT (e.g. an id_token)
   * @param string $jwt
   * @return bool
   */
  public function verifyJWT(string $jwt): bool
  {

    list($header, $payload, $signature) = explode(".", $jwt);

    $header = json_decode(JWT::urlsafeB64Decode($header), true);

    if (empty($header)) {
      throw new \RuntimeException('Sorry, token provided is not a valid JWT');
    }

    $algo = ['RS256', $header['alg']];
    
    JWT::$leeway = $this->leeway;

    return (bool) JWT::decode($jwt, new Key( $this->getPubkey(), $header['alg'] ));
  }

  /**
   * Validate whether a JWT token is valid.
   * @param string $jwt
   * @return bool
   */
  public function isJWT(string $jwt): bool
  {

    list($header, $payload, $signature) = explode(".", $jwt);

    if (empty($header) || !json_decode(JWT::urlsafeB64Decode($header))) {
      return false;
    }

    if (empty($payload) || !json_decode(JWT::urlsafeB64Decode($payload))) {
      return false;
    }

    if (empty($signature)) {
      return false;
    }

    return true;
  }

  /**
   * Find the user id associated with this keycloak token (usually based on email address)
   * @param string    $jwt
   * @return object   Decoded JWT object containing sub and email
   */
  public function decodeToken(string $jwt): ?object
  {

    list($header, $payload, $signature) = explode(".", $jwt);

    $header = json_decode(JWT::urlsafeB64Decode($header), true);

    if (empty($header)) {
      throw new \RuntimeException('Sorry, token provided is not a valid JWT');
    }

    $algo = ['RS256', $header['alg']];

    JWT::$leeway = $this->leeway;
    
    $decoded = JWT::decode($jwt, new Key( $this->getPubkey(), $header['alg'] ));

    // Attempt to retrieve a user from the email, or preferred_username (which is assumed to be email) in the token
    $email = $decoded->email;
    if (empty($email)) {
      $email = $decoded->preferred_username; // No email, but we configure keycloak to return email as username, but not always
    }
    $email = filter_var($email, FILTER_VALIDATE_EMAIL); // Ensure after this we actually have an email address

    if (empty($email)) {
      throw new \RuntimeException('No email address could be found in OIDC token');
    }

    return $decoded;
  }

  /**
   * Parse authorization code into JWT access token
   *
   * @param string $authorizationCode
   * @return string|null  Java web token
   */
  public function parseCode(string $authorizationCode): ?string
  {
    // Get access token for the user
    try {
      $accessToken = $this->getAccessToken('authorization_code', ['code' => $authorizationCode]);
      // Get user profile data
      $profile = $this->getResourceOwner($accessToken);
      // Deconstruct the OIDC access token
      $values = $accessToken->getValues();
    } catch (IdentityProviderException $e) {
      throw new authException("[{$e->getMessage()}]: {$e->getResponseBody()['error_description']}");
    }

    if (empty($values['id_token'])) {
      throw new authException('ID token not found in OIDC response');
      return null;
    }

    $jwt = $values['id_token'];
    if (!$this->isJWT($jwt)) {
      throw new authException('ID token was not a JWT');
      return null;
    }
    if (!$this->verifyJWT($jwt)) {
      throw new authException('JWT could not be verified');
      return null;
    }

    return $jwt;
  }

  abstract protected function getPubKey(): string;
}
