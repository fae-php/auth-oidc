<?php

namespace FAE\auth_oidc;

use Symfony\Contracts\Cache\ItemInterface;

use FAE\cache\cache_adapter;

class keycloak extends oidcAbstract
{

  protected $clientId;
  protected $clientSecret;
  public $baseurl;
  public $keycloakRealm;
  public $scopes;

  /**
   * Construct a keycloak interface
   * Array $settings passes configuration of your keycloak authentication server
   * 
   *   array $settings 
   *     string keycloakRealm (required) The target keycloak realm
   *     string baseurl       (required) The base URL of the keycloak authentication server
   *     string clientId      (required) The ID of the client configured in keycloak
   *     string scopes        (optional) A space separated list of scopes to request from the keycloak endpoint
   *     string clientSecret  (optional) The secret of the client configured in keycloak
   * 
   * @param array $settings
   */
  public function __construct(array $settings)
  {
    $this->keycloakRealm  = $settings['keycloakRealm'];
    $this->baseurl        = rtrim($settings['baseurl'], '/') . '/';
    $this->clientId       = $settings['clientId'];
    $this->clientSecret   = $settings['clientSecret'];
    $this->scopes         = $settings['scopes'] ?: ['openid profile email'];

    if (!$this->keycloakRealm) {
      throw new \Exception("Keycloak realm not defined, this is required");
    }

    if (!$this->baseurl) {
      throw new \Exception("Keycloak authentication server base URL not defined, this is required");
    }

    if (!$this->clientId) {
      throw new \Exception("Keycloak client ID not defined, this is required");
    }

    parent::__construct(
      [
        'clientId' => $this->clientId,
        'clientSecret' => $this->clientSecret,
        'redirectUri' =>  $this->getRedirectUrl(),
        'urlAuthorize' => $this->getAuthURL(),
        'urlAccessToken' => $this->getTokenURL(),
        'urlResourceOwnerDetails' => $this->getProfileURL(),
        'scopes' => $this->scopes,
        'scopesSeparator' => ' '
      ]
    );
  }

  public function getRedirectUrl(): string
  {
    global $config;
    if(empty($_SESSION['keycloak_redirect_url'])){
      $_SESSION['keycloak_redirect_url'] = "{$config->root}{$config->path}/api/{$config->apiVersion}/auth_oidc/keycloak/authorize";
    }
    return $_SESSION['keycloak_redirect_url'];
  }

  public function getAuthURL(): string
  {
    return $this->baseurl . "auth/realms/{$this->keycloakRealm}/protocol/openid-connect/auth";
  }

  public function getTokenURL(): string
  {
    return $this->baseurl . "auth/realms/{$this->keycloakRealm}/protocol/openid-connect/token";
  }

  public function getProfileURL(): string
  {
    return $this->baseurl . "auth/realms/{$this->keycloakRealm}/protocol/openid-connect/userinfo";
  }

  public function getLogoutURL(): string
  {
    return $this->baseurl . "auth/realms/{$this->keycloakRealm}/protocol/openid-connect/logout";
  }

  /**
   * Return the public key used to sign tokens from keycloak
   * @return string
   */
  public function getPubkey(): string
  {

    if (defined('KEYCLOAK_PUBLICKEY_' . $this->keycloakRealm)) {
      return constant('KEYCLOAK_PUBLICKEY_' . $this->keycloakRealm);
    }

    $cacheAdapter = new cache_adapter(cache_adapter::LOW);
    $cacheInstance = $cacheAdapter->getCache();

    return $cacheInstance->get('keycloak_publickey', function (ItemInterface $item) {

      $item->expiresAfter(3600);

      $json = json_decode(file_get_contents($this->baseurl . 'auth/realms/' . $this->keycloakRealm), true);
      if (empty($json)) {
        throw new \RuntimeException("Could not retrieve public key to validate your request, please try again");
      }
        
      $pubkey = "-----BEGIN PUBLIC KEY-----\n";
      $pubkey .= $json['public_key'];
      $pubkey .= "\n-----END PUBLIC KEY-----";
      
      return $pubkey;
    });
  }

  /**
   * Retrieve the current certificate from keycloak
   * @return string
   */
  public function getCertificate(): string
  {

    if (defined('KEYCLOAK_CERTIFICATE_' . $this->keycloakRealm)) {
      return constant('KEYCLOAK_CERTIFICATE_' . $this->keycloakRealm);
    }

    $cacheAdapter = new cache_adapter(cache_adapter::LOW);
    $cacheInstance = $cacheAdapter->getCache();

    return $cacheInstance->get('keycloak_certificate', function (ItemInterface $item) {

      $item->expiresAfter(3600);

      $json = json_decode(file_get_contents($this->baseurl . 'auth/realms/' . $this->keycloakRealm . '/protocol/openid-connect/certs'), true);
      if (empty($json)) {
        throw new \RuntimeException("Could not retrieve certificate to validate your request, please try again");
      }
        
      $cert = "-----BEGIN CERTIFICATE-----\n";
      $cert .= $json['keys'][0]['x5c'][0];
      $cert .= "\n-----END CERTIFICATE-----";
      
      return $cert;
    });
  }
}
